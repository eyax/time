//
//  main.cpp
//  Time
//
//  Created by Hébriel ROUSSEAU on 11/02/2019.
//  Copyright © 2019 eyax. All rights reserved.
//

#include <iostream>
#include "Time.hpp"

int main(int argc, const char * argv[])
{
    ey::Clock clock;
    
    ey::Time time = ey::Time::seconds(4.6); //create a time object set to 4.6 seconds
    time += ey::Time::milliseconds(140); //add 140 ms to the object
    
    while(true)
    {
        std::cout << clock.restart().asSeconds() << std::endl; //deltatime in seconds
    }
    return 0;
}
