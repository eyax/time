# Time

Simple Clock and Time classes, inspired from the SFML API. Those classes uses C++11 std::chrono features and can be precise to the nanosecond.